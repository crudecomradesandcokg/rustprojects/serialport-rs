use serialport::{available_ports, SerialPortType};

#[macro_use]
extern crate fstrings;

fn main() {
    match available_ports() {
        Ok(ports) => {
            println!("Found port{}:", ports.len());

            for p in ports {
                f!("  {p.port_name}");

                match p.port_type {
                    SerialPortType::UsbPort(info) => {
                        println!(
                            "    Type: USB\n    VID:{:04x} PID:{:04x}",
                            info.vid, info.pid
                        );
                        print_optional_field("     Serial Number", &info.serial_number);
                        print_optional_field("      Manufacturer", &info.manufacturer);
                        print_optional_field("           Product", &info.product);
                    }
                    SerialPortType::BluetoothPort => {
                        println!("    Type: Bluetooth");
                    }
                    SerialPortType::PciPort => {
                        println!("    Type: PCI");
                    }
                    SerialPortType::Unknown => {
                        println!("    Type: Unknown");
                    }
                }
            }
        }
        Err(e) => {
            eprintln!("{:?}", e);
            eprintln!("Error listing serial ports");
        }
    }
}

fn print_optional_field(label: &str, value: &Option<String>) {
    println!("    {}: {}", label, value.as_deref().unwrap_or(""));
}
